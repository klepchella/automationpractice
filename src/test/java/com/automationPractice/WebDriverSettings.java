package com.automationPractice;

import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;

public class WebDriverSettings {
    public ChromeDriver driver;

    @BeforeMethod
    public void setUp() {

        // Здесь сто процентов можно не задавать путь к драйверу хардкодом...
        System.setProperty("webdriver.chrome.driver",
                "D:\\documents\\programming\\java\\chromedriver_win32\\chromedriver.exe");
        driver = new ChromeDriver();
    }

    @AfterMethod
    public void setDown() {
        driver.quit();
    }
}
