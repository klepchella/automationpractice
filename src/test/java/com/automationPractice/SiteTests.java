package com.automationPractice;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.Select;
import org.testng.Assert;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import java.util.Random;
import java.util.concurrent.TimeUnit;


public class SiteTests extends WebDriverSettings {

    public WebElement emailInput;

    @BeforeMethod
    @Override
    public void setUp() {
        super.setUp();

        driver.get("http://automationpractice.com/index.php");
        Assert.assertTrue(this.checkTitle("My Store"));

        WebElement logInHeader = driver.findElement(By.className("header_user_info"));

        logInHeader.click();

        driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);

        Assert.assertTrue(this.checkTitle("Login - My Store"));

        emailInput = driver.findElement(By.id("email_create"));

    }

    @Test(dataProvider = "NotEmail")
    public void fourthTest(String locale, String expected) {

        emailInput.sendKeys(locale);

        WebElement buttonCreate = driver.findElement(By.id("SubmitCreate"));
        buttonCreate.click();

        WebElement invalidErrorField = driver.findElement(By.id("create_account_error"));
        WebElement invalidErrorMsg = invalidErrorField.findElement(By.tagName("li"));

        Assert.assertEquals(invalidErrorMsg.getText(), expected);

    }

    @DataProvider(name = "NotEmail")
    public static Object[][] createNotEmails() {

        String invalidMsg = "Invalid email address.";
        return new Object[][]{
                {"Text", invalidMsg},
                {"Tex5462135t", invalidMsg},
                {"", invalidMsg},
                {"aaaaa@", invalidMsg},
                {"132165@fogkg", invalidMsg},
        };
    }

    public boolean checkTitle(String titleTemplate) {
        String title = driver.getTitle();
        return title.equals(titleTemplate);
    }

    @Test
    public void firstTest() {

        Random r = new Random();
/*      не очень хороший способ задания тестовых данных,
        но для успешной регистрации email нужен каждый раз новый
 */
        String email = "123" + r.nextInt() + "qwgf@fhgnd.com";

        emailInput.sendKeys(email);

        WebElement buttonCreate = driver.findElement(By.id("SubmitCreate"));
        buttonCreate.click();

        driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);
        WebElement createAccountPageHeader = driver.findElement(By.className("navigation_page"));

        //        проверяем, что попали на страницу ввода персональной информации
        Assert.assertTrue(createAccountPageHeader.getText().toLowerCase().equals("authentication"));

        //        first_name
        WebElement firstName = driver.findElement(By.id("customer_firstname"));
        firstName.sendKeys("Ivan");

        //        last_name
        WebElement lastName = driver.findElement(By.id("customer_lastname"));
        lastName.sendKeys("Ivanov");
        
        //        passwd
        WebElement passwd = driver.findElement(By.id("passwd"));
        passwd.sendKeys("zxcvb");

        //        address1
        WebElement address1 = driver.findElement(By.id("address1"));
        address1.sendKeys("Balonina, Vologograd, Russia");

        //        city
        WebElement city = driver.findElement(By.id("city"));
        city.sendKeys("Volgograd");
        
        //        state
        WebElement stateId = driver.findElement(By.id("id_state"));
        Select state = new Select(stateId);
        state.selectByValue("1");
        
        //        postcode
        WebElement postcode = driver.findElement(By.id("postcode"));
        postcode.sendKeys("78745");
        
        //        id_country
        WebElement idCountry = driver.findElement(By.id("id_country"));
        Select country = new Select(idCountry);
        country.selectByValue("21");

        //        phone_mobile
        WebElement phoneMobile = driver.findElement(By.id("phone_mobile"));
        phoneMobile.sendKeys("+96511320489");

        WebElement submitAccount = driver.findElement(By.id("submitAccount"));
        submitAccount.click();

        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
        Assert.assertTrue(this.checkTitle("My account - My Store"));

    }

}
